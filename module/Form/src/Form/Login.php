<?php

    namespace Form\Form;

    use Zend\Form\Form;
    use Zend\Form\Element;
    use Zend\InputFilter;
    use Zend\Filter;

    class Login extends Form{
        public function __construct()
        {
            parent::__construct();
            $this->loginForm(); // định nghĩa form
            $this->loginInputFilter(); // định nghĩa filter
        }
        private function LoginForm(){
            $username = new Element\Text('username');
            $username->setLabel('Username: ')->setLabelAttributes([
                'for'=>'username',
                'class'=>'label-control'
            ])->setAttributes([
                'id'=>'username',
                'class'=>'form-control is-invalid',
                'placeholder'=>'Nhập username',
            ]);
            
            $this->add($username);

            $password = new Element\Password('password');
            $password->setLabel('Password:')->setLabelAttributes([
                'for'=>'password',
                'class'=>'label-control'
            ])->setAttributes([
                'id'=>'password',
                'class'=>'form-control is-invalid',
            ]);

            $this->add($password);

            $remember = new Element\Checkbox('remember');
            $remember->setLabel('Remember me: ')->setLabelAttributes([
                'for'=>'remember',
                'class'=>'label-control'
            ])->setAttributes([
                'id'=>'remember',
                'value'=>1,
                'required'=>false
            ]);
                $this->add($remember);
            
            $this->add([
                    'name'=>'buttonSubmit',
                    'type'=>Element\Submit::class,
                    'attributes'=>[
                        'id'=>'btnSubmit',
                        'class'=>'btn btn-primary',
                        'value'=>'Đăng nhập'
                    ],
            ]);

            
        }

        private function loginInputFilter(){
          $inputFilter = new InputFilter\InputFilter();
         // $inputFilter = new Zend\Filter();
            $this->setInputFilter($inputFilter);

            $inputFilter->add([
                    'name'=>'username',
                    'required'=>true,
                    'filter'=>[
                        ['name'=>'StringToLower'],
                        ['name'=>'StringTrim'],
                    ],
                    'validators'=>[
                        [
                            'name'=>'StringLength',
                            'options'=>[ //Nơi bắt validate
                                'messages'=>[
                                    \Zend\Validator\StringLength::TOO_SHORT=>'Username ít nhất 6 kí tự.',
                                    \Zend\Validator\StringLength::TOO_LONG=>'Username không quá 20 kí tự.',
                                    \Zend\Validator\StringLength::INVALID=>'Không đúng định dạng'
                                ]
                            ]
                        ],
                        [
                            'name'=>'Regex',
                            'options'=>[
                                'pattern'=>'/[a-zA-Z\d!@#$%^&*.]/',
                                'messages'=>[
                                \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                                \Zend\Validator\Regex::NOT_MATCH=>'Chỉ cho phép nhập chữ và số.',
                                \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                            ]
                            ]
                        ]

                    ]
            ]);
            $inputFilter->add([
                'name'=>'password',
                'required'=>true,
                'filter'=>[
                    ['name'=>'StringToLower'],
                    ['name'=>'StringTrim'], // xóa các khoảng trắng đầu và cuối dòng
                    ['name'=>'StripTags'], // xóa các tags
                    ['name'=>'StripNewlines'], // xóa các new line
                ],
                'validators'=>[
                    [
                        'name'=>'StringLength',
                        'options'=>[
                            'min'=>6,
                            'max'=>20,
                            'messages'=>[
                                \Zend\Validator\StringLength::TOO_SHORT=>'Password quá ngắn, ít nhất %min% kí tự.',
                                \Zend\Validator\StringLength::TOO_LONG=>'Password quá dài, tối đa %max% kí tự.',
                                \Zend\Validator\StringLength::INVALID=>'Không đúng định dạng'
                            ]
                        ]
                    ],
                    [
                        'name'=>'Regex',
                        'options'=>[
                            'pattern'=>'/[a-z]/',
                            'messages'=>[
                                \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                                \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự thường.',
                                \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                            ]
                        ]
                    ],
                    [
                        'name'=>'Regex',
                        'options'=>[
                            'pattern'=>'/[A-Z]/',
                            'messages'=>[
                                \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                                \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 kí tự hoa.',
                                \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                            ]
                        ]
                    ],
                    [
                        'name'=>'Regex',
                        'options'=>[
                            'pattern'=>'/\d/',
                            'messages'=>[
                                \Zend\Validator\Regex::INVALID=>'Không đúng định dạng.',
                                \Zend\Validator\Regex::NOT_MATCH=>'Bắt buộc phải chứa ít nhất 1 số.',
                                \Zend\Validator\Regex::ERROROUS=>'Lỗi không xác định.'
                            ]
                        ]
                    ],

                ]
        ]);


        }
    }
?>