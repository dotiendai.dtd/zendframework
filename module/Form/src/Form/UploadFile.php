<?php

namespace Form\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use Zend\Filter;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Validator;
use Zend\Validator\File\Size;
use Zend\Validator\File\MimeType;
use Zend\ValidatorChain;

class UploadFile extends Form{

    public function __construct()
    {
        parent::__construct();

        $this->add([
            'name'=>'fileUpload',
            'attributes'=>[
                'type'=>'file',
                'multiple'=>false, // False: Upload 1 file, True: Upload nhiều file
            ],
            'options'=>[
                'label'=>'Choose File: ',
            ]
        ]);

        $this->add([
            'name'=>'buttonSubmit',
            'type'=>Element\Submit::class,
            'attributes'=>[
                'id'=>'btnSubmit',
                'class'=>'btn btn-success',
                'value'=>'Upload'
            ],
        ]);

        $this->validateForUpload();
    }

    public function validateForUpload(){
        $fileUpload = new Input('fileUpload');

        $fileUpload->setRequired(true);
        
        //fileSize
        $size = new Size(['max'=>2*1024*1024]); //2mb
        $size->setMessages([
            Size::TOO_BIG=>'File you choosed so big, please again',
        ]);

        //mimeType : png, jpeg, jpg
        $mimeType = new MimeType(array('image/png','image/jpeg','image/jpg'));
        $mimeType->setMessages([
            MimeType::FALSE_TYPE=>'File you choosed false type ',
            MimeType::NOT_DETECTED=>'File you choosed Undefine',
            MimeType::NOT_READABLE=>'Can not read'
        ]);

        $fileUpload->getValidatorChain()
        ->attach($size, true, 2)
        ->attach($mimeType, true, 1);

        $inputFilter = new InputFilter($fileUpload);
        $inputFilter->add($fileUpload);
        $this->setInputFilter($inputFilter);

    }
}