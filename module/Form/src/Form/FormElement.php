<?php

namespace Form\Form;

use Zend\Captcha;
use Zend\Form\Element;
use Zend\Form\Fieldset;
use Zend\Form\Form;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use phpDocumentor\Reflection\Types\Self_;
use Abc;

class FormElement Extends Form{

    public function __construct()
    {
        parent::__construct();

        //text
        $fullname = new Element('fullname');
        $fullname->setLabel('Fullname: ');
        $fullname->setLabelAttributes([
            'for'=>'fullname',
            'class'=>'control-label'
        ]);
        $fullname->setAttributes([
            'id'=>'fullname',
            'type'=>'text',
            'class'=>'form-control',
            'placeholder'=>'Nhap ho va ten: '
        ]);

        $this->add($fullname);

        //hidden
        $hidden = new Element('hidden-input');
        $hidden->setAttributes([
            'type'=>'hidden',
            'value'=>'zendFrameWork'
        ]);
        $this->add($hidden);

        //number
        $age = new Element\Number('age');
        $age->setLabel('Age: ');
        $age->setLabelAttributes([
            'for'=>'age',
            'class'=>'control-label'
        ]);
        $age->setAttributes([
            'min'=>18,
            'max'=>50,
            'class'=>'form-control',
            'value'=>18,
            'id'=>'age'
        ]);
        $this->add($age);

        //email
        $email = new Element\Email('email');
        $email->setLabel('Email: ')->setLabelAttributes([
            'for'=>'email',
            'class'=>'label-control'
        ])->setAttributes([
            'placeholder'=>'Nhap email',
            'required'=>true,
            'class'=>'form-control',
            'id'=>'email'
        ]);
        $this->add($email);

        //username 
        $username = new Element\Text('username');
        $username->setLabel('Username: ')->setLabelAttributes([
            'for'=>'username',
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'username',
            'class'=>'form-control',
            'minLength'=>6,
            'maxLength'=>20,
            'placeholder'=>'Nhập username',
            'required'=>true
        ]);

        //password
        $pass = new Element\Password('password');
        $pass->setLabel('Password: ')->setLabelAttributes([
            'for'=>'password',
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'password',
            'class'=>'form-control',
            'placeholder'=>'Nhap mat khau',
            'minLength'=>6,
            'maxLength'=>20,
            'required'=>true
        ]);
        $this->add($pass);

        //radio
        $gender = new Element\Radio('gender');
        $gender->setLabel('Gender: ')->setLabelAttributes([
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'gender',
            'value'=>'male',
            'style'=>'margin-left: 20px'
        ]);
        $gender->setValueOptions([
            'male'=>' Male',
            'female'=>' Female',
            'other'=> ' Other Gender'
        ]);
        $this->add($gender);

        //select option
        $subject = new Element\Select('subject');
        $subject->setLabel('Subject: ')->setLabelAttributes([
            'class'=>'lable-control'
        ]);
        $subject->setAttributes([
            'id'=>'subject',
            'class'=>'form-control'
        ]);
        $subject->setValueOptions([
            'html/css'=>' Html/Css',
            'jquery'=>' Jquery',
            'bootstrap'=>' Bootstrap 4',
            'php'=> 'PHP 7.x',
            'laravel'=>'Laravel 5.7',
            'zend'=>'Zend 3'
        ]);
        $this->add($subject);

        //textarea
        $message = new Element\Textarea('message');
        $message->setLabel('Message: ')->setLabelAttributes([
            'for'=>'message',
            'class'=>'label-control'
        ])->setAttributes([
            'id'=>'message',
            'class'=>'form-control',
            'row'=>5,
            'minLength'=>10,
            'style'=>'resize:none'
        ]);
        $this->add($message);

        //files
        $file = new Element\File('files');
        $file->setLabel('Choose image: ')->setLabelAttributes([
            'for'=>'files'
        ])->setAttributes([
            'multiple'=>true,
        ]);
        $this->add($file);

        //checkbox
        $favorites = new Element\MultiCheckbox('favorites');
        $favorites->setLabel('Your Favorites: ')->setLabelAttributes([
            'class'=>'label-control',
        ])->setAttributes([
            'id'=>"favorites",
            'style'=>'margin-left: 20px'
            //'checked'=true,
            //'value'=>['music','game']
        ]);
        $favorites->setValueOptions([
            'music'=>' Music',
            'game'=>' Game',
            'book'=>' Read book', 
            'girl'=>' Love',
        ]);
        $this->add($favorites);

        //color
        $this->add([
            'name'=>'color',
            'type'=>Element\Color::class,
            'options'=>[
                'label'=>'Choose Color: '
            ],
            'attributes'=>[
                'value'=>'#ABC123',
                'id'=>'color'
            ]
        ]);

        //date
        $this->add([
            'name'=>'birthday',
            'type'=>Element\Date::class,
            'attributes'=>[
                'class'=>'form-control',
                'id'=>'birthday'
            ],
            'options'=>[
                'label'=>'Choose Birthday Date',
                'class'=>'label-control'
            ]
        ]);

        //range
        $this->add([
            'name'=>'range',
            'type'=>'range',
            'attributes'=>[
                'min'=>5,
                'max'=>20,
                'value'=>'15',
                'class'=>'form-control',
                'id'=>'range'
            ],
            'options'=>[
                'label'=>'Choose value: ',
                'class'=>'control-label',
              //  'for'=>'range'
            ]
        ]);


        //button Reset
        $this->add([
            'name'=>'buttonReset',
            'type'=>Element\Button::class,
            'attributes'=>[
                'type'=>'reset',
                'id'=>'btnReset',
                'class'=>'btn btn-primary',
            ],
            'options'=>[
                'label'=>'Reset'
            ]
        ]);

        //button Submit type=>Element\Submit::class
        $this->add([
            'name'=>'buttonSubmit',
            'type'=>Element\Submit::class,
            'attributes'=>[
                'id'=>'btnSubmit',
                'class'=>'btn btn-success',
                'value'=>'Send'
            ],
        ]);

         // var_dump(Abc\Constants::RESOURCE_LINK); die; // self:: goi class hien tai
        //Captcha image
        $this->add([
            'type'=>'captcha',
            'name'=>'captcha_image',
            'options'=>[
                'label'=>'Enter String Below: ',
                'captcha'=>[
                    'class'=>'Image',
                    'imgDir'=>'public/img/captcha', // Duong dan chinh xac luu duong dan
                    'imgUrl'=> Abc\Constants::RESOURCE_LINK.'/img/captcha/', // Duong dan hinh anh hien thi o browser
                    'suffix'=>'.png',
                    'font'=>APPLICATION_PATH.'/data/font/PressStart2P-Regular.ttf',
                    'fsize'=>50,
                    'width'=>400,
                    'height'=>150,
                    'dotNoiseLevel'=>200,
                    'lineNoiseLevel'=>5,
                    'expiration'=>120 //120s
                ]
            ]
        ]);
    }
}
?>