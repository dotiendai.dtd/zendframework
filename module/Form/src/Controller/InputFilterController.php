<?php

namespace Form\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Form\Form\Login;
use Zend\InputFilter\Input;
use Zend\InputFilter\InputFilter;
use Zend\Ldap\Ldap;
use PHPUnit\Framework\Constraint\Exception;

class InputFilterController extends AbstractActionController{
    
    public function indexAction(){

        $form = new Login(); 
        $checkRequest = $this->getRequest();
        if($checkRequest->isPost()){
            $data = $this->params()->fromPost();
           // print_r($data); die;
            $form->setData($data);
            if($form->isValid()){
                $data = $form->getData();
               // print_r($data); die;
                $usernameRq = $data['username'];
                $passwordRq = $data['password'];
               
              
                    $options = [
                        'host'              => '192.168.0.103',
                        'username'          => 'monitor@itl.com', //'CN=user1,DC=foo,DC=net',
                        'password'          => 'Itlus3r@2013',
                        'bindRequiresDn'    => false,
                        'accountDomainName' => 'itl.com',
                        'baseDn'            => 'OU=Central Management,DC=itl,DC=com',
                    ];
                   
                $ldap = new Ldap($options);

                $ldap->bind($usernameRq, $passwordRq);
                //  print_r($ldap->bind($usernameRq, $passwordRq)); die;
                
                $acctname = $ldap->getCanonicalAccountName($usernameRq, Ldap::ACCTNAME_FORM_DN);
                if($acctname){
                    echo "$acctname\n";die;
                }else{
                    dd('error cmnr');
                }
             
            }
            // else{
            //     $err = $form->getMessages(); 
                // foeach($err as $err)...
            // }
        }
        
        return new ViewModel([
            'form'=>$form
        ]);
    }
}