<?php

namespace Form\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Form\Form\FormElement;
use ABC\Constant\Constant;
use Zend\Validator\ValidatorInterface;
use Zend\Validator\StringLength;
use Zend\Validator\Regex;
use Zend\Validator\File\Extension;
use Zend\Validator\ValidatorChain;

class ValidatorChainController extends AbstractActionController{
    function indexAction(){

        $string = new StringLength(['min'=>6,'max'=>20]);
        $string->setMessages([
            StringLength::INVALID=>'Dữ liệu bạn nhập không hợp lệ',
            StringLength::TOO_SHORT=>'Dữ liệu bạn nhập quá ngắn',
            StringLength::TOO_LONG=>'Dữ liệu bạn nhập quá dài',
        ]);

        $regex = new Regex('/[a-zA-Z0-9]/');
        $regex->setMessages([
            Regex::INVALID=>'Pattern không đúng',
            Regex::NOT_MATCH=>"Dữ liệu không hợp lệ cho '%pattern%'",
            Regex::ERROROUS=>"Lỗi nội bộ khi sử dụng '%pattern'",
        ]);
        $regexSysbol = new Regex('/[!@#$%^&*.;:]/');
        $regexSysbol->setMessages([
            Regex::INVALID=>'Pattern không đúng',
            Regex::NOT_MATCH=>"Dữ liệu không hợp lệ cho '%pattern%'",
            Regex::ERROROUS=>"Lỗi nội bộ khi sử dụng '%pattern'",
        ]);

        $validatorChain = new ValidatorChain(); //ValidatorChain = Cho phép validator nhiều loại
        $validatorChain->attach($string,true,3);// mức độ ưu tiên 3 -2 -1
        $validatorChain->attach($regex,true,2); // true thì break nếu gặp lỗi;
        $validatorChain->attach($regexSysbol,true,1);


        $form = new FormElement;
        $checkRequest = $this->getRequest();

        if($checkRequest->isPost()){
            $value = $this->params()->fromPost();
           // print_r($value) ; die;
            if($validatorChain->isValid($value['password'])){
                echo $value['password'];
            }else{
                $message = $validatorChain->getMessages();
                foreach($message as $err){
                    echo $err."</br>";
                }
            }
        }

       return new ViewModel(['form'=>$form]);
        
    }
}