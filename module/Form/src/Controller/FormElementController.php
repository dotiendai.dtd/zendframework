<?php

namespace Form\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Form\Form\FormElement;
use ABC\Constant\Constant;

class FormElementController extends AbstractActionController
{
    public function indexAction()
    {
      //  print_r($_SERVER); die;
        $form = new FormElement();
        $view = new ViewModel([
            'form'=>$form,
        ]);
        return $view;
    }

    function getDataAction(){
        $check = $this->getRequest();

        $form = new FormElement();
        if($check->isPost()){
            $data = $this->params()->fromPost();
            $file = $check->getFiles();
            echo "<pre>";
            print_r($data);
            echo "<br>";
            print_r($file);
            echo "</pre>";
        }

        $view = new ViewModel(['form'=>$form]);
        $view->setTemplate('form/form-element/get-data'); // set đường dẫn
        return $view;

    }

    function index02Action(){
        $check = $this->getRequest();

        $form = new FormElement();
        if($check->isPost()){
            $data = $this->params()->fromPost();
            $file = $check->getFiles();
            echo "<pre>";
            print_r($data);
            echo "<br>";
            print_r($file);
            echo "</pre>";
        }

        $view = new ViewModel(['form'=>$form]);
        $view->setTemplate('form/form-element/index02'); // set đường dẫn
        return $view;

    }
   
}
