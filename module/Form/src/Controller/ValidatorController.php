<?php

namespace Form\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Form\Form\FormElement;
use ABC\Constant\Constant;
use Zend\Validator\ValidatorInterface;
use Zend\Validator\StringLength;
use Zend\Validator\Regex;
use Zend\Validator\File\Extension;

class ValidatorController extends AbstractActionController{
        //stringlength
        function stringAction(){
            $validator = new StringLength(['min'=>8,'max'=>20]);
            //$validator = new StringLength(['min'=>8]);

            $var = "Dai Tien Do";
            if($validator->isValid($var)){
                echo $var;
            }else{
                $message = $validator->getMessages();
                echo current($message);
                // foreach($message as $err){
                //     echo $err."<br>";
                // }
            }

            return false;
            
        }

        //between
        function numberAction(){
            $validator = new \Zend\Validator\Between([
                'min'=>5,
                'max'=>10,
                'inclusive'=>true // Cho phép lấy giá trị biên. False thì k lấy.
            ]);
            $var = 5;
            if($validator->isValid($var)){
                echo $var;
            }else{
                $message = $validator->getMessages();
                echo current($message);
            }
            return false;
        }

        //Date(['format'=>'d-m-Y'])
        //EmailAddress()
        //Digits() //Số tự nhiên
        //GreaterThan(['min'=>10,'inclusive'=>true]) // So sánh lớn hơn hoặc bằng (inclusive true)
        //LessThan(['max'=>10,'inclusive'=>true]) // So sánh bé hơn hoặc bằng (inclusive true)
        //\Zend\Validator\File\Exists()
        //$file = APPLICATION_PATH."/public/files/checkFile.txt";
        function inArrayAction(){
            $validator = new \Zend\Validator\InArray([
                'haystack'=>[
                    'key1'=>['value1','value2','value3',1,2,3,4,'valueN'],
                    'key2'=>[5,6,7,8,9]
                ],
                'strict'=>\Zend\Validator\InArray::COMPARE_NOT_STRICT_AND_PREVENT_STR_TO_INT_VULNERABILITY,
                'recursive'=>true //Nhận giá trị từng phần tử trong đệ quy, nhưng k nhận nguyên mảng
            ]);
            $var = '5';
            if($validator->isValid($var)){
                print_r($var);
                die;
            }else{
                $message = $validator->getMessages();
                echo current($message);
               // die;
            }
            return false;

        }

        //regex
        function regexAction(){
            $validator = new Regex([
                'pattern'=>'/^Zend/'
            ]);
          //  $var = 'Zend FrameWork 3'; // true (sZend : false, zend : false)

            // $pattern = "/^[\d]{5}$/"; //Cho pheps laf so, bat buoc la 5 ki tu
            // $validator->setPattern($pattern);
            // $var = '12312';

            $pattern = '/^[a-zA-Z ]*$/';
            $validator->setPattern($pattern);
            $value = "Do tien dai";
              //  echo $value;
            if($validator->isValid($value)){
                echo $value;
            }else{
                $err = $validator->getMessages();
                echo current($err);
            }
            return false;
        }

        function fileExtensionAction(){
            // $validator = new Extension([
            //     'extension'=>['php','png','txt','jpg'],
            //     'case'=>false // Có phân biệt hoa thường
            // ]);
            $validator = new Extension(['txt','php','jpg','png']);

            $file = APPLICATION_PATH."/public/files/checfdfdkFile.txt";

            if($validator->isValid($file)){
                echo "Exactly";
            }else{
                $message = $validator->getMessages();
                echo current($message);
            }
            return false;
        }

        //passwordstrength
        function passwordStrengthAction(){
            $validator = new \Zend\Validator\PasswordStrength();

            $pass = 'Dotiendai1996';
            if($validator->isValid($pass)){
                echo "pass thoa man";
            }else{
                $message = $validator->getMessages();
                foreach($message as $err){
                    echo $err."<br>";
                }
            }
            return false;
        }
}