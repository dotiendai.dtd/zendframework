<?php

namespace DBConnect\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Platform\PlatformInterface;

class AdapterController extends AbstractActionController{

    function AdapterDB(){
        $adapter = new Adapter([
            'driver'=>'Pdo_Mysql',
            'database'=>'db_zendframework',
            'username'=>'root',
            'password'=>'',
            'hostname'=>'localhost',
            'charset'=>'utf8'
        ]);
        return $adapter;
    }

    function indexAction(){
        $database = $this->AdapterDB();

        $sql = "select * from foods where id = ?";
        $statement = $database->query($sql);
        $result = $statement->execute([5]);


      foreach($result as $row){
          echo "<pre>";
            print_r($row);
          echo"</pre>";
      }
        return false;
    }

    function createStatementAction(){

        $database = $this->AdapterDB();

        //$sql = "select * from food_type where id>?";
       // $statement = $database->createStatement($sql,[10]);

        //$sql = "select * from foods where id between ? and ?";
        //$statement = $database->createStatement($sql,[5, 12]);

        // $sql = "select * from food_type where name like :ten"; //Cách 2
        // $statement = $database->createStatement($sql, [
        //     'ten'=>'%canh%',
        // ]);
        $sql = "select * from food_type where name like ?";
        $statement = $database->createStatement($sql, ['%canh%',]);
        $rs = $statement->execute();
        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
          echo"</pre>";
        }

        return false;
    }

    function platformDemoAction(){
        $db = $this->AdapterDB();
        $qi = function ($name) use ($db) {
            return $db->platform->quoteIdentifier($name);
        };
        $fp = function ($name) use ($db) {
            return $db->driver->formatParameterName($name);
        };
        // $sql = 'UPDATE '.$qi('food_type').'SET '.$qi('name'). ' = '.$fp('NAME')
        // .' WHERE '.$qi('id').' = '.$fp('ID');

        // $statement=$db->query($sql);
        // $parameter = [
        //     'NAME' => 'Món ăn mỗi ngày',
        //     'ID' => 4
        // ];
        // $statement->execute($parameter);
        
        $sql2 = 'SELECT * FROM '.$qi('food_type').' WHERE '.$qi('id').' = '.$fp('ID').' OR '.$qi('id').' = '.$fp('ID_2');
        $statement = $db->query($sql2);
        $parameter = [
            'ID' => 4,
            'ID_2' => 9
        ];
        $rs = $statement->execute($parameter);
        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;
    }


}