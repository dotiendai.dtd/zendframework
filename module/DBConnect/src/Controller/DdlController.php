<?php

namespace DBConnect\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Platform\PlatformInterface;
use Zend\Db\Sql\Ddl;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Ddl\Column;
use Zend\Db\Sql\Ddl\Constraint;

class DdlController extends AbstractActionController{
    function AdapterDB(){
        $adapter = new Adapter([
            'driver'=>'Pdo_Mysql',
            'database'=>'db_zendframework',
            'username'=>'root',
            'password'=>'',
            'hostname'=>'localhost',
            'charset'=>'utf8'
        ]);
        return $adapter;
    }

    //create Table
    function createTableAction(){
        $adapter = $this->AdapterDB();

        $table = new Ddl\CreateTable('demo');
        $table->addColumn(new Column\Integer('id'));
        $table->addConstraint(new Constraint\PrimaryKey('id'));
        //$table->setOption('AUTO_INCREMENT', true);
        $table->addColumn(new Column\Varchar('name', 20));
        $table->addConstraint(
            new Constraint\UniqueKey(['name'], 'my_unique_key')
        );
        $sql = new Sql($adapter);

        $adapter->query(
            $sql->getSqlStringForSqlObject($table),
            $adapter::QUERY_MODE_EXECUTE
        );

        echo "<br>";
        echo "create complete";
        echo "<br>";
      //  echo $sql->getSqlStringForSqlObject($table);
        return false;
    }

    //alter Table
    function alterTableAction(){
        $adapter = $this->AdapterDB();
        $table = new Ddl\AlterTable('demo');
        $table->addColumn(new Column\Varchar('email', 50));
        $table->changeColumn('name', new Column\Varchar('new_name', 50));
        $sql = new Sql($adapter);

        $adapter->query(
            $sql->getSqlStringForSqlObject($table),
            $adapter::QUERY_MODE_EXECUTE
        );

        echo "<br>";
        echo "create complete";
        echo "<br>";
        echo $sql->getSqlStringForSqlObject($table);
        return false;
    }

    //create Table 
    function createTable2Action(){
        $adapter = $this->AdapterDB();

        $table = new Ddl\CreateTable('demo_2');
        $table->addColumn(new Column\Integer('id'));
        $table->addConstraint(new Constraint\PrimaryKey('id'));
        //$table->setOption('AUTO_INCREMENT', true);
        $table->addColumn(new Column\Integer('id_type'));
        $table->addConstraint(
            new Constraint\ForeignKey('fk_demo_demo2','id_type','demo','id')
        );
        $sql = new Sql($adapter);

        $adapter->query(
            $sql->getSqlStringForSqlObject($table),
            $adapter::QUERY_MODE_EXECUTE
        );

        echo "<br>";
        echo "create complete";
        echo "<br>";
      //  echo $sql->getSqlStringForSqlObject($table);
        return false;
    }

    //drop Table
    function dropTableAction(){
        $adapter = $this->AdapterDB();

        $drop = new Ddl\DropTable('demo');

        $sql = new Sql($adapter);

        $adapter->query(
            $sql->getSqlStringForSqlObject($drop),
            $adapter::QUERY_MODE_EXECUTE
        );

        echo "<br>";
        echo "drop complete";
        echo "<br>";
      //  echo $sql->getSqlStringForSqlObject($table);
        return false;
    }
}