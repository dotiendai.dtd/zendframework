<?php

namespace DBConnect\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Platform\PlatformInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Where;

class SqlController extends AbstractActionController{
    function AdapterDB(){
        $adapter = new Adapter([
            'driver'=>'Pdo_Mysql',
            'database'=>'db_zendframework',
            'username'=>'root',
            'password'=>'',
            'hostname'=>'localhost',
            'charset'=>'utf8'
        ]);
        return $adapter;
    }

    //select basic
    function selectAction(){
        $adapter = $this->AdapterDB();
        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from('foods');
        $select->where(['id'=>2]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }

    }

    //columns, as
    function select02Action(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(['f'=>'foods']);
        $select->columns(['idsp'=>'id','tensp'=>'name','nlieu'=>'summary']);
        $select->where(['f.id'=>1]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;
    }

    //join
    function select03Action(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);

        $select = $sql->select();
        $select->from(['f'=>'foods']);
       $select->columns(['idsp'=>'id','tensp'=>'name','nlieu'=>'summary']);
        $select->where(['f.id'=>1]);
        $select->join(['t'=>'food_type'],'f.id_type = t.id',['tenloai'=>'name'],$select::JOIN_INNER);

        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;
    }

    //where
    function select04Action(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('foods');
        //c1: where name like '%canh%'
        // $select->where(function(Where $where){
        //     $where->like('name','%canh%');
        // });
        //c2: where id = 10
        //$select->where('id = 10');
        //c3: new \Zend\Db\Sql\Predicate\
        //$select->where(new \Zend\Db\Sql\Predicate\In('id',[1,2,3]));
        //$select->where(new \Zend\Db\Sql\Predicate\Between('id',5,7));
        //$select->where(new \Zend\Db\Sql\Predicate\Like('name','%súp%'));
        $select->where(new \Zend\Db\Sql\Predicate\Expression("id = ? OR id = ?",[2, 10]));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;

    }

    //order() //litmit(5)->offset(1)
    function select05Action(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('foods');
        $select->order('id','DESC');// order('price DESC','id ASC')
        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;

    }

    //group()
    // tinhs toongr soos mons awn mooix loaij, sau do lay cac loai cos so luong lon hon 10

    //select maloai, tenloai, count(idsp) from food_type t join foods f on t.id=f.id_type group by t.id
    function select06Action(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(['t'=>'food_type']);
        $select->columns(['maloai'=>'id','tenloai'=>'name','tongsoSP'=>new \Zend\Db\Sql\Predicate\Expression("count(f.id)")]);
        $select->join(['f'=>'foods'],'t.id = f.id_type',[],$select::JOIN_LEFT);
        $select->group(['t.id','t.name'])->having('tongsoSP > 8');
        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;

    }

    //count(f.id), avg(f.price), 
    //min(f.price), max(f.price), sum(f.price), 
    //UPPER(f.name), concat('id', ' - ','name') theo tung loai
    //group_concat(f.name SEPARATOR '; ')

    function select07Action(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(['t'=>'food_type']);
        $select->columns([
            'maloai'=>'id',
            'tenloai'=>'name',
            'dongiaTrungBinh'=> new \Zend\Db\Sql\Expression("avg(f.price)"),
            'group_concat' => new \Zend\Db\Sql\Expression("group_concat(f.name SEPARATOR '; ')"),
            ]);
        $select->join(['f'=>'foods'],'t.id = f.id_type',[],$select::JOIN_LEFT);
        $select->group(['t.id','t.name']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;

    }

    //join multi table
    function select08Action(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(['f'=>'foods']);
        $select->join(['d'=>'menu_detail'],'f.id = d.id_food',[], $select::JOIN_INNER)
        ->join(['m'=>'menu'],'m.id = d.id_menu',[],$select::JOIN_INNER);
        $select->where('m.id = 2');

        // $query = "select f.* from foods f join menu_detail d on f.id = d.id_food join menu m on m.id = d.id_menu where m.id = 2";
        // $statement = $adapter->query($query);
        // $result = $statement->execute();
        // foreach($result as $row){
        //     echo "<pre>";
        //     print_r($row);
        //     echo"</pre>";
        // }
        // die;
        //$sql->getSqlStringForSqlObject($select)

        $statement = $sql->prepareStatementForSqlObject($select);
        $rs = $statement->execute();

        foreach($rs as $row){
            echo "<pre>";
            print_r($row);
            echo"</pre>";
        }
        return false;

    }

    //insert
    function insertAction(){
        $adapter = $this->AdapterDB();

        $sql = new Sql($adapter);

        $insert = $sql->insert('customers');

        $insert->values([
            'name'=>'DDoox Tieends DDaij',
            'gender'=>'Name',
            'email'=>'agf.mrhandsome@gmail.com',
            'address'=>'quan 2',
            'phone'=>'01903232232',
            'note'=>'Bad Boy'
        ]);

        $statement = $sql->prepareStatementForSqlObject($insert);
        $results = $statement->execute();

        echo "insert complete";
        echo "<br>";
        echo $sql->getSqlStringForSqlObject($insert);
        return false;
    }

    //update
    function updateAction(){
        $adapter = $this->AdapterDB();
        $sql = new Sql($adapter);

        $update = $sql->update('customers');
        $update->set([
            'phone'=>'0902445377',
            'address'=>'Quaaanj 9'
        ]);
        $update->where('id = 21');
        $statement = $sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        echo "update complete";
        echo "<br>";
        echo $sql->getSqlStringForSqlObject($update);
        return false;
    }

    //delete
    function deleteAction(){
        $adapter = $this->AdapterDB();
        $sql = new Sql($adapter);
        $delete = $sql->delete('customers')->where('id = 21');

        $statement = $sql->prepareStatementForSqlObject($delete);
        $results = $statement->execute();
        echo "delete complete";
        echo "<br>";
        echo $sql->getSqlStringForSqlObject($delete);
        return false;
    }

}