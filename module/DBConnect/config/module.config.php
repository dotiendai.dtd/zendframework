<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace DBConnect;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'adapter' => [
                'type'    => Segment::class,
                'options' => [
                  //  'route'    => '/dbconnect', // ten controller
                    'route'=> '/adapter[/:action]',
                    'defaults' => [
                        //'__NAMESPACE__'=>'DBConnect\Controller',
                        //'controller' => 'Adapter',
                        'controller' => Controller\AdapterController::class,
                        'action'     => 'index',
                    ],
                ],
                // 'may_terminate'=>true,
                // 'child_routes'=>[
                //     'sub'=>[
                //         'type'=> Segment::class,
                //         'options'=>[
                //             'route'=>'/[:controller][/:action]',
                //             'constraints'=>[
                //                 'controller'=>'[a-zA-Z][a-zA-Z0-9_-]*',
                //                 'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                //             ],
                //         ],
                //     ],
                // ],
            ],
            'sql' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/sql[/:action]', // ten controller
                    'defaults' => [
                        'controller' => Controller\SqlController::class,
                        'action'     => 'select',
                    ],
                ],
            ],
            'ddl' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/ddl[/:action]', // ten controller
                    'defaults' => [
                        'controller' => Controller\DdlController::class,
                        'action'     => 'createTable',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\AdapterController::class => InvokableFactory::class,
            Controller\SqlController::class => InvokableFactory::class,
            Controller\DdlController::class => InvokableFactory::class,
        ],
        // 'invokables'=>[
        //     'Controller\AdapterController'=>\DBConnect\Controller\AdapterController::class,
        //     'Controller\SqlController'=>\DBConnect\Controller\SqlController::class,
        // ],
        // 'aliases'=>[
        //     'Adapter'=>'Controller\AdapterController',
        //     'Sql'=>'Controller\SqlController',
        // ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
