<?php

namespace Started;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'started_route_1' => [ // name routes
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/started',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                    // 'constrains'=>[
                    //     'action'=>'[a-zA-Z0-9]*',
                    //     'id'=>'[0-9]*'
                    // ]
                ],
                'may_terminate'=>false,
                'child_routes'=>[
                    'sub_route_1'=>[ //name child route
                        'type'=>Segment::class,
                        'options'=>[
                            'route'=>'[/:action][/:id]',
                            'defaults'=>[
                                'controller'=>Controller\LoginController::class,
                                'action'=>'home',
                            ]
                        ],
                        'constrains'=>[
                        'action'=>'[a-zA-Z0-9]*',
                        'id'=>'[0-9]*'
                        ]
                    ]
                ]
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
           Controller\IndexController::class => InvokableFactory::class,
           Controller\LoginController::class => InvokableFactory::class,
           // Controller\IndexController::class => \Started\Controller\Factory\IndexControllerFactory::class
        ],
        // 'invokables'=>[
        //     'LoginController'=> \Started\Controller\LoginController::class
        // ],
        // 'aliases'=>[
        //     'Login'=>'LoginController' // Login trên key defaults->controller của Routes
        // ]
    ],
    'view_manager' => [
        // 'display_not_found_reason' => true,
        // 'display_exceptions'       => true,
        // 'doctype'                  => 'HTML5',
        // 'not_found_template'       => 'error/404',
        // 'exception_template'       => 'error/index',
        // 'template_map' => [
        //     'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
        //     'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
        //     'error/404'               => __DIR__ . '/../view/error/404.phtml',
        //     'error/index'             => __DIR__ . '/../view/error/index.phtml',
        // ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];